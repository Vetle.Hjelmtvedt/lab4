package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return this.getGrid().numRows();
	}

	@Override
	public int numberOfColumns() {
		return this.getGrid().numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return this.getGrid().get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		// Iterate through every cell and set state to getNextCell
		for (int row = 0; row < nextGeneration.numRows(); row++) {
			for (int col = 0; col < nextGeneration.numColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		// Count living neighbours first
		int livingNeighbours = countNeighbors(row, col, CellState.ALIVE);


		// Rules for living cell
		if (this.getGrid().get(row, col) == CellState.ALIVE) {
			// Dead if less than two living neighbours
			if (livingNeighbours < 2) {
				return CellState.DEAD;
			// Alive if two or three neighbours
			} else if (livingNeighbours == 2 || livingNeighbours == 3) {
				return CellState.ALIVE;
			// Dead if more than three neighbours, only remaining case
			} else {
				return CellState.DEAD;
			}

			

		// Rules for dead cell
		} else {
			// Alive if exactly three neighbourrs
			if (livingNeighbours == 3) {
				return CellState.ALIVE;
			} else {
				return CellState.DEAD;
			}

		}
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		
		int counter = 0;

		for (int rowIndex = row-1; rowIndex <= row+1; rowIndex++) {
			for (int colIndex = col-1; colIndex <= col+1; colIndex++) {
				try {
					if (rowIndex == row && colIndex == col) {
						continue;
					}
					if (this.getGrid().get(rowIndex, colIndex) == state) {
						counter++;
					}
				} catch (IndexOutOfBoundsException e) {
					continue;
				}
			}
		}


		return counter;
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
