package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    /**
     * The grid of cells
     */
    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }




    /**
     * Get the state of the cell in the provided row and column
     *
     * @param row    The row of the cell, 0-indexed
     * @param column The column of the cell, 0-indexed
     * @return The state of the cell in the given row and column.
     */
    @Override
    public CellState getCellState(int row, int column) {
        return this.getGrid().get(row, column);
    }

    /**
     * Sets the start-state for each cell
     */
    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    /**
     * Updates the state of the cell according to the rules of the automaton
     */
    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        // Iterate through every cell and set state to getNextCell
        for (int row = 0; row < nextGeneration.numRows(); row++) {
            for (int col = 0; col < nextGeneration.numColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }
        this.currentGeneration = nextGeneration;
    }

    /**
     * Given the current state of the cell and the number of alive neighbours,
     * decide the next state of the cell.
     *
     * @param row The row of the cell, 0-indexed
     * @param col
     * @return state of cell for next generation
     */
    @Override
    public CellState getNextCell(int row, int col) {

        // Count living neighbours first
        int livingNeighbours = countNeighbors(row, col, CellState.ALIVE);


        // Rules for living cell
        if (this.getGrid().get(row, col) == CellState.ALIVE) {
            // Now dying
            return CellState.DYING;

        // Rules for dying cell
        } else if (this.getGrid().get(row, col) == CellState.DYING) {
            return CellState.DEAD;

        // Rules for dead cell
        } else {
            // Alive if two neighbours
            if (livingNeighbours == 2) {
                return CellState.ALIVE;
            } else {
                return CellState.DEAD;
            }
        }
    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {

        int counter = 0;

        for (int rowIndex = row-1; rowIndex <= row+1; rowIndex++) {
            for (int colIndex = col-1; colIndex <= col+1; colIndex++) {
                try {
                    if (rowIndex == row && colIndex == col) {
                        continue;
                    }
                    if (this.getGrid().get(rowIndex, colIndex) == state) {
                        counter++;
                    }
                } catch (IndexOutOfBoundsException e) {
                    continue;
                }
            }
        }


        return counter;
    }

    /**
     * @return The number of rows in this automaton
     */
    @Override
    public int numberOfRows() {
        return this.getGrid().numRows();
    }

    /**
     * @return The number of columns in this automaton
     */
    @Override
    public int numberOfColumns() {
        return this.getGrid().numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
