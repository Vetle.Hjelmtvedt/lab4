package datastructure;

import java.util.Arrays;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int columns;
    private int rows;
    private CellState cellStates[][];

    public CellGrid(int rows, int columns, CellState initialState) {
        this.columns = columns;
        this.rows = rows;
        // Init array
        this.cellStates = new CellState[rows][columns];
        // Fill it with initialState
        Arrays.stream(this.cellStates).forEach(a -> Arrays.fill(a, initialState));
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // Check that row and column args are not out of bounds
        if (!(row >= 0 && row < this.numRows()))
            throw new IndexOutOfBoundsException();
        if (!(column >= 0 && column < this.numColumns()))
            throw new IndexOutOfBoundsException(); 
        this.cellStates[row][column] = element;
        
    }

    // code duplication er underrated

    @Override
    public CellState get(int row, int column) {
        // Check that row and column args are not out of bounds
        if (!(row >= 0 && row < this.numRows()))
            throw new IndexOutOfBoundsException();
        if (!(column >= 0 && column < this.numColumns()))
            throw new IndexOutOfBoundsException();
        return this.cellStates[row][column];
    }



    @Override
    public IGrid copy() {
        CellGrid gridKopi = new CellGrid(this.numRows(), this.numColumns(), CellState.ALIVE);

        // Iterate over this grid
        for (int row = 0; row < this.numRows(); row++) {
            for (int col = 0; col < this.numColumns(); col++) {
                gridKopi.set(row, col, this.get(row, col));
            }
        }

        return gridKopi;
    }
    
}
